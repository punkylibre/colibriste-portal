# Building...

FROM golang:alpine as server-builder

WORKDIR /colibriste

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh build-base
ADD . .
RUN go version
RUN go get -d -v
RUN go test ./...
RUN go build

# Running...

FROM alpine

WORKDIR /app

RUN apk update && apk add ca-certificates libcap mailcap
# ca-certificates for autocert (Let's Encrypt) and mailcap to get mime types for downloaded documents

COPY --from=server-builder /colibriste/colibriste-portal /app
COPY --from=server-builder /colibriste/dev_certificates /app/dev_certificates
COPY --from=server-builder /colibriste/web /app/web
COPY --from=server-builder /colibriste/configs /app/configs

RUN setcap cap_net_bind_service=+ep colibriste-portal

ENTRYPOINT [ "./colibriste-portal"]
