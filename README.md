# Colibriste

Ce portail est le portail d'accueil de la plateforme libre Colibriste, dévelopé en [GO](https://golang.org/) avec la précieuse aide de l'ORM [GORM](https://gorm.io/).

## Fonctionnalités

Les principales fonctionnalités :

- Listing des applications disponibles sur le portail suivant les groupes de l'utilisateur connecté. Il y a trois typologie de groupes "Grand Public" pour les utilisateurs non connectés, "Membres" pour les simples utilisateurs connectés et "Admin" pour les utilisateurs admin de la plateforme
- Admministration des outils sur le portail : pour les admins connectés, une page permet d'administrer les outils présents sur le portail.

## Démarrage pour tests / dev

### Avec VS Code

> Il est nécessaire d'avoir un environnement de développement GO installé et opérationnel sur le poste.

Télécharger le dépot

```sh
git clone https://gitlab.com/punkylibre/colibriste-portal.git
```

Ouvrir le dépôt avec VS Code puis dans l'onglet `Debug`, démarrer le projet avec `Debug colibriste with Mock OAuth2`

La démo est accessible avec l'url https://colibriste.127.0.0.1.nip.io:1443

### Avec Docker

Installer sur le poste [Docker](https://docs.docker.com/get-docker/) et [docker-compose](https://docs.docker.com/compose/install/)

Télécharger le dépot

```sh
git clone https://gitlab.com/punkylibre/colibriste-portal.git
cd colibriste-portal
```

Dans le fichier docker-compose.yml décommenter la ligne `command: -debug` (ATTENTION : cette ligne doit être commentée lors d'un passage en prod et ne sert que pour tester ou débuger l'application)

```sh
docker-compose up -d
```

La démo est accessible avec l'url https://colibriste.127.0.0.1.nip.io

## Architecture

### Arborescence fichier

- `./data` contient les bases de données locales sqlite, users.db pour les utilisateurs et test.db pour le modèle de donnée
- `./dev-certificates` contient les fichiers nécessaires pour servir l'application en HTTPS durant le développement.
- `./internal` contient les paquets nécessaires au développement de l'application
  - `./auth` contient la gestion des utilisateurs et des droits sur l'application
  - `./mocks` permet de mocker l'API d'authentification pour passer les tests et s'authentifier avec OIDC sans OIDC provider
  - `./models` permet de définir le modèle de données et les API qui seront servies par le back-end
  - `./rootmux` contient le fichier `rootmux.go` qui permet de servir les APIs, de gérer l'authentification, de servir le site web et la gestion des cookies utilisateurs. Le répertoire contient également tous les tests d'intégration sur les APIs.
- `./miscellaneous/styles` contient le code scss qui peut être minifié et intégré à l'appli
- `./pkg` contient les différents package génériques qui permettent de gérer les logs applicatifs, de créer les fonctiones de tests, la gestion des tokens...
- `./web` est le répertoire où est stockée l'application front-end et publié par le serveur back-end.

### Utilisateurs et droits
**Utilisateurs techniques**
Les utilisateurs techniques permettent de s'authentifier à l'application et d'accéder aux API en fonction du rôle de l'utilisateur qui définit alors ses droits.
La source de création et d'authentification des utilisateurs technique est double. Elle peut soit provenir directement de la base de donnée locale d'utilisateurs de l'application (./data/users.db) ou d'un fournisseur d'identité avec le protocole OAuth2.

Dans le cas d'une connexion avec OAuth2, l'utilisateur est crée dans la base locale d'utilisateurs avec son identifiant OAuth2 qui permet de faire le lien avec l'utilisateur technique. Dans le cas d'une connexion avec OAuth2, l'utilisateur doit obligatoirement faire parti d'un groupe applicatif autorisé (cf variable d'envrionnement ADMIN_GROUP et MEMBER_GROUP) Un rôle différent est attribué automatiquement à l'utilisateur en fonction de son groupe.

> ATTENTION : le rôle "ADMIN" est obligatoire dans l'application car permet de gérer les utilisateurs et leurs droits. Sans celui-ci et sans utilisateurs ayant ce rôle dans la base, il est impossible de créer de nouveaux utilisateurs !

**Utilisateurs applicatif**
Ils permettent d'affecter des données à un utilisateur. Les utilisateurs applicatifs sont créé dans la base locale de données (./data/test.db) avec l'identifiant de l'utilisateur technique qui permet de faire la jointure entre un utilisateur applicatif et un utilisateur technique.

### Tests

**Tests unitaires**
Les tests unitaires sont définis dans le répertoire de chaque paquet (ex : `auth-test.go` dans le package `auth`)

**Tests d'intégration**
Les tests d'intégration  sont dans le répertoire `./rootmux`. 
Les tests métiers (ex : mettre à jour un outil en tant qu'admin) sont définis dans le fichier `rootmux_test.go`.  
Les tests concernant les droits et les réponses attendues par utilisateur (ex : un utilisateur non connecté n'a accès qu'aux applis "Grand Public") sont écrits dans des fichiers différent par type de droits différent (ex : `unlogged_test.go` pour tester les droits des non connectés)

### Design

Pour modifier le design de l'application sans en altérer le code source,  récupérer le répertoire `miscellaneous/styles` en local.


Dans le répertoire `miscellaneous/styles`  exécuter la commande `npm run build-perso` (nécessite npm d'installer sur le poste) pour construire le fichier minifié CSS prenant en compte la personnalisation. 

Placer ensuite ce fichier à côté du fichier `docker-compose.yml` et décommenter la ligne appropriée dans le fichier `docker-compose.yml` puis exécuter la commande `docker-compose up -d` pour lancer le conteneur.

En mode développement, dans le répertoire `miscellaneous/styles`. Modifier le scss à votre convenance et exécuter `npm run build`.

## Contribution

Un bug découvert ? Une demande d'amélioration ? Une contribution à la documentation ?

Utilisé le système d'issue pour expliquer votre découverte ou votre poposition.

Vous voulez contribuez directement au code ?

Créer une issue pour expliquer votre contribution, créer une branche à partir de cette issue, développer votre fonctionnalité sur la branche et faite une merge request.
