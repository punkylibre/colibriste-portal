package models

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/punkylibre/colibriste-portal/internal/auth"
)

// HandleClients expose the UserClients API
func (d *DataHandler) HandleApplication(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(strings.TrimPrefix(r.URL.Path, "/api/Application/"))
	switch method := r.Method; method {
	case "GET":
		switch auth.GetLoggedUserTechnical(w, r).Role {
		case "ADMIN":
			d.getAppsAdmin(w, r, id)
		case "MEMBER":
			d.getAppsMember(w, r, id)
		default:
			d.getAppsUnlogged(w, r, id)
		}
	case "POST":
		switch auth.GetLoggedUserTechnical(w, r).Role {
		case "ADMIN":
			d.postAppsAdmin(w, r)
		default:
			http.Error(w, "method not allowed", 405)
		}
	case "PUT":
		switch auth.GetLoggedUserTechnical(w, r).Role {
		case "ADMIN":
			d.putAppsAdmin(w, r, id)
		default:
			http.Error(w, "method not allowed", 405)
		}
	case "DELETE":
		switch auth.GetLoggedUserTechnical(w, r).Role {
		case "ADMIN":
			d.deleteApplication(w, r, id)
		default:
			http.Error(w, "method not allowed", 405)
		}
	default:
		http.Error(w, "method not allowed", 405)
	}
}

func (d *DataHandler) getAppsUnlogged(w http.ResponseWriter, r *http.Request, id int) {
	if id != 0 {
		http.Error(w, "method not allowed", 405)
	} else {
		var o []Application
		d.db.Where("\"group\" = ?", "other").Order("Group").Order("Order").Find(&o)
		json.NewEncoder(w).Encode(o)
	}
}

func (d *DataHandler) getAppsMember(w http.ResponseWriter, r *http.Request, id int) {
	if id != 0 {
		http.Error(w, "method not allowed", 405)
	} else {
		var o []Application
		d.db.Where("\"group\" = ? OR \"group\" = ?", "other", "member").Order("Group").Order("Order").Find(&o)
		json.NewEncoder(w).Encode(o)
	}
}

func (d *DataHandler) getAppsAdmin(w http.ResponseWriter, r *http.Request, id int) {
	if id != 0 {
		var o Application
		if err := d.db.Where(reqID, id).First(&o).Error; err != nil {
			http.Error(w, ErrorIDIsMissing, http.StatusNotFound)
			return
		}
		json.NewEncoder(w).Encode(o)

	} else {
		var o []Application
		d.db.Where(`"group" = ? OR "group" = ? OR "group" = ?`, "other", "member", "admin").Order("Group").Order("Order").Find(&o)
		json.NewEncoder(w).Encode(o)
	}
}

func (d *DataHandler) postAppsAdmin(w http.ResponseWriter, r *http.Request) {
	var o Application
	err := json.NewDecoder(r.Body).Decode(&o)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var applications []Application
	d.db.Find(&applications)
	for _, val := range applications {
		if o.Name == val.Name {
			http.Error(w, "Name Already exist.", 409)
			return
		}

		if o.Url == val.Url {
			http.Error(w, "URL Already exist.", 409)
			return
		}
	}

	var flag = false
	for _, group := range groups {
		if group == o.Group {
			flag = true
		}
	}
	if !flag {
		http.Error(w, "Group don't exist.", 409)
		return
	}

	d.db.Create(&o)
	d.db.Last(&o)
	json.NewEncoder(w).Encode(o)

}

func (d *DataHandler) putAppsAdmin(w http.ResponseWriter, r *http.Request, id int) {
	var o Application
	if err := d.db.First(&o, id).Error; err != nil {
		http.Error(w, ErrorIDIsMissing, http.StatusNotFound)
		return
	}

	var application Application
	err := json.NewDecoder(r.Body).Decode(&application)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var applications []Application
	d.db.Find(&applications)
	for _, val := range applications {
		if o.ID != val.ID {
			if application.Name == val.Name {
				http.Error(w, "Name Already exist.", 409)
				return
			}

			if application.Url == val.Url {
				http.Error(w, "URL Already exist.", 409)
				return
			}
		}
	}

	var flag = false
	for _, group := range groups {
		if group == application.Group {
			flag = true
		}
	}
	if !flag {
		http.Error(w, "Group don't exist.", 409)
		return
	}

	o.Name = application.Name
	o.Order = application.Order
	o.Group = application.Group
	o.Url = application.Url
	o.FaIcon = application.FaIcon
	o.Description = application.Description
	d.db.Save(&o)
	json.NewEncoder(w).Encode(o)

}

func (d *DataHandler) deleteApplication(w http.ResponseWriter, r *http.Request, id int) {
	if id != 0 {
		var o Application
		if err := d.db.First(&o, id).Error; err != nil {
			http.Error(w, ErrorIDIsMissing, http.StatusNotFound)
			return
		}

		d.db.Delete(&o)
	} else {
		http.Error(w, ErrorIDIsMissing, http.StatusNotFound)
	}
}
