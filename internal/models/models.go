package models

import (
	"net/http"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/punkylibre/colibriste-portal/internal/auth"

	// Needed for sqlite
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// DataHandler init a gorm DB an presents API handlers
type DataHandler struct {
	db *gorm.DB
}

// ErrorIDDoesNotExist = "id does not exist" with 404 http.StatusNotFound
const ErrorIDDoesNotExist = "id does not exist"

// ErrorIDIsMissing = "id is missing" with 404 http.StatusNotFound
const ErrorIDIsMissing = "id is missing"

// ErrorCannotAccessRessource = "You can not access this ressource" with 403 http.StatusForbidden
const ErrorCannotAccessRessource = "You can not access this ressource"

// ErrorRoleOfLoggedUser = "Could not get role of logged user" with 500 http.StatusInternalServerError
const ErrorRoleOfLoggedUser = "Could not get role of logged user"

// ErrorNotAuthorizeMethodOnRessource = "You're not authorize to execute this method on this ressource." with 405 http.StatusMethodNotAllowed
const ErrorNotAuthorizeMethodOnRessource = "You're not authorize to execute this method on this ressource."

// ErrorUserIDIsMissing = "id of UserClient is missing"
const ErrorUserIDIsMissing = "id of UserClient is missing"

const reqID = "id = ?"
const reqUserID = "user_id = ?"
const reqIDAndBankerID = "id = ? and user_banker_id = ?"

var groups = [...]string{"admin", "member", "other"}

// NewDataHandler init a DataHandler and returns a pointer to it
func NewDataHandler() *DataHandler {
	db, err := gorm.Open("sqlite3", "./data/test.db")
	if err != nil {
		panic("failed to connect database")
	}
	db.LogMode(true)

	// Migrate the schema
	db.AutoMigrate(&Application{})
	return &DataHandler{db: db}
}

// ProcessAPI redirect API call to DataHandler to each correct API func
func (d *DataHandler) ProcessAPI(w http.ResponseWriter, r *http.Request) {
	api := strings.Split(strings.TrimPrefix(r.URL.Path, "/api/"), "/")[0]
	switch api {
	case "Application":
		d.HandleApplication(w, r)
	}
}

// UserBanker has many UserClient
type Application struct {
	ID          uint       `gorm:"primary_key"`
	CreatedAt   time.Time  `json:"-"`
	UpdatedAt   time.Time  `json:"-"`
	DeletedAt   *time.Time `json:"-"`
	Order       int
	Name        string `gorm:"not null;unique"`
	Description string
	Group       string
	Url         string
	FaIcon      string
}

func (d *DataHandler) getLoggedUser(w http.ResponseWriter, r *http.Request) interface{} {
	user := auth.GetLoggedUserTechnical(w, r)
	return user
}
