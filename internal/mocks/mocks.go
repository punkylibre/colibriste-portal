// Package mocks provide mocks for development purposes (debug mode)
package mocks

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/punkylibre/colibriste-portal/pkg/middlewares"
)

const literralContentType = "Content-Type"
const literralApplicationJson = "application/json"

var (
	hostname = os.Getenv("HOSTNAME")
	port     string
)

// Init initialize the configuration
func Init(portFromMain int) {
	port = strconv.Itoa(portFromMain)
}

// CreateMockOAuth2 creates a mock OAuth2 serve mux for development purposes
func CreateMockOAuth2() *http.ServeMux {
	mux := http.NewServeMux()
	// Returns authorization code back to the user
	mux.HandleFunc("/auth", func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		redir := query.Get("redirect_uri") + "?state=" + query.Get("state") + "&code=mock_code"
		http.Redirect(w, r, redir, 302)
	})
	// Returns authorization code back to the user, but without the provided state
	mux.HandleFunc("/auth-wrong-state", func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		redir := query.Get("redirect_uri") + "?state=" + "a-random-state" + "&code=mock_code"
		http.Redirect(w, r, redir, 302)
	})

	// Returns access token back to the user
	mux.HandleFunc("/token", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(literralContentType, "application/x-www-form-urlencoded")
		w.Write([]byte("access_token=mocktoken&scope=user&token_type=bearer"))
	})
	// Returns userinfo back to the user
	mux.HandleFunc("/userinfo", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(literralContentType, literralApplicationJson)
		w.Write([]byte(`{
			"name": "Us ER",
			"email": "user@test.fr",
			"memberOf": ["MEMBER"],
			"sub": "1",
			"login": "USER"
		}`))
	})
	// Returns userinfo back to the user (with an admin user)
	mux.HandleFunc("/admininfo", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(literralContentType, literralApplicationJson)
		w.Write([]byte(`{
			"name": "Ad MIN",
			"email": "admin@test.fr",
			"memberOf": ["ADMIN","MEMBER"],
			"sub": "1",
			"login": "ADMIN"
		}`))
	})
	// Logout
	mux.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Logout OK")
	})

	return mux
}

// CreateMockAPI creates a mock OAuth2 serve mux for development purposes
func CreateMockAPI() *http.ServeMux {
	mux := http.NewServeMux()
	// Returns authorization code back to the user
	frameSource := "https://static." + hostname + ":" + port
	mux.Handle("/", middlewares.Cors(func() http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set(literralContentType, literralApplicationJson)
			w.Write([]byte(`{
				"foo": "bar",
				"bar": "foo"
			}`))
		})
	}(), frameSource))
	return mux
}
