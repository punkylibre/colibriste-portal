package auth

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/punkylibre/colibriste-portal/pkg/common"
	"gitlab.com/punkylibre/colibriste-portal/pkg/log"
	"gitlab.com/punkylibre/colibriste-portal/pkg/tokens"
	"golang.org/x/crypto/bcrypt"
)

var (
	//UsersFile is the file containing the users
	UsersFile = "./configs/users.json"
)

// HandleInMemoryLogin validate the username and password provided in the function body against a local file and return a token if the user is found
func (m Manager) HandleInMemoryLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "method not allowed", 405)
		return
	}
	var sentUser User
	err := json.NewDecoder(r.Body).Decode(&sentUser)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	// Try to match the user with an user in the database
	d := NewDataHandler()
	user, err := d.MatchUser(sentUser)
	if err != nil {
		http.Error(w, err.Error(), 403)
		log.Logger.Printf("| %v | Login failure | %v | %v", sentUser.Login, r.RemoteAddr, log.GetCityAndCountryFromRequest(r))
		return
	}
	// Store the user in cookie
	// Store only the relevant info
	// Generate
	xsrfToken, err := common.GenerateRandomString(16)
	if err != nil {
		http.Error(w, "error generating XSRF Token", 500)
		return
	}
	tokenData := TokenData{User: User{ID: user.ID, Login: user.Login, Role: user.Role}, XSRFToken: xsrfToken}
	tokens.Manager.StoreData(tokenData, m.Hostname, authTokenKey, 24*time.Hour, w)
	// Log the connexion
	log.Logger.Printf("| %v (%v %v) | Login success | %v | %v", user.Login, user.Name, user.Surname, r.RemoteAddr, log.GetCityAndCountryFromRequest(r))
}

// ByID implements sort.Interface for []User based on the ID field
type ByID []User

func (a ByID) Len() int           { return len(a) }
func (a ByID) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByID) Less(i, j int) bool { return a[i].ID < a[j].ID }

// ProcessUsers processes users regarding of HTTP method
func ProcessUsers(w http.ResponseWriter, req *http.Request) {
	d := NewDataHandler()
	switch method := req.Method; method {
	case "GET":
		d.SendUsers(w, req)
	case "POST":
		d.AddUser(w, req)
	case "PUT":
		d.UpdateUser(w, req)
	case "DELETE":
		d.DeleteUser(w, req)
	default:
		http.Error(w, "method not allowed", 400)
	}
}

// SendUsers send users as response from an http requests
func (d *DataHandler) SendUsers(w http.ResponseWriter, req *http.Request) {
	var users = d.getUsers()
	json.NewEncoder(w).Encode(users)
}

// AddUser adds an user
func (d *DataHandler) AddUser(w http.ResponseWriter, req *http.Request) {
	var users = d.getUsers()
	if req.Body == nil {
		http.Error(w, "please send a request body", 400)
		return
	}
	var newUser User
	err := json.NewDecoder(req.Body).Decode(&newUser)
	if _, ok := err.(*json.UnmarshalTypeError); !ok && err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	// Check login don't already exist
	for _, val := range users {
		if newUser.Login == val.Login {
			http.Error(w, "login already exists", 400)
			return
		}
	}

	// Encrypt the password with bcrypt
	if newUser.Password == "" {
		http.Error(w, "passwords cannot be blank", 400)
		return
	} else {
		hash, err := bcrypt.GenerateFromPassword([]byte(newUser.Password), bcrypt.DefaultCost)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}
		newUser.PasswordHash = string(hash)
		newUser.Password = ""
	}
	if newUser.Role == "ADMIN" {
		newUser.IsAdmin = true
	} else {
		newUser.IsAdmin = false
	}

	d.createUser(newUser)
	d.db.Last(&newUser)
	json.NewEncoder(w).Encode(newUser)

}

// UpdateUser update an user
func (d *DataHandler) UpdateUser(w http.ResponseWriter, req *http.Request) {
	id, _ := strconv.Atoi(strings.TrimPrefix(req.URL.Path, "/users/"))
	if id != 0 {
		user := d.getUser(id)
		if req.Body == nil {
			http.Error(w, "please send a request body", http.StatusBadRequest)
			return
		}
		var newUser User
		err := json.NewDecoder(req.Body).Decode(&newUser)
		if _, ok := err.(*json.UnmarshalTypeError); !ok && err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		// check that login doesn't already exist
		if user.Login != newUser.Login {
			var users = d.getUsers()
			for _, val := range users {
				if newUser.Login == val.Login {
					http.Error(w, "login already exists", 400)
					return
				}
			}
		}
		user.IDOAuth = newUser.IDOAuth
		user.Login = newUser.Login
		user.DisplayName = newUser.DisplayName
		user.Name = newUser.Name
		user.Surname = newUser.Surname
		user.Role = newUser.Role
		if user.Role == "ADMIN" {
			user.IsAdmin = true
		} else {
			user.IsAdmin = false
		}
		if newUser.Password != "" {
			hash, err := bcrypt.GenerateFromPassword([]byte(newUser.Password), bcrypt.DefaultCost)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			user.PasswordHash = string(hash)
			user.Password = ""
		}
		d.saveUser(user)
		json.NewEncoder(w).Encode(user)
	} else {
		http.Error(w, "Please provide an ID", http.StatusBadRequest)
		return
	}
}

// DeleteUser remove an user
func (d *DataHandler) DeleteUser(w http.ResponseWriter, req *http.Request) {
	pathElements := strings.Split(req.URL.Path, "/")
	idx, err := strconv.Atoi(pathElements[len(pathElements)-1])
	if err != nil {
		http.Error(w, "please provide an user index", 400)
		return
	}
	err = d.removeUser(idx)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

// MatchUser attempt to find the given user against users in configuration file
func (d *DataHandler) MatchUser(sentUser User) (User, error) {
	var emptyUser User
	var users = d.getUsers()
	for _, user := range users {
		if user.Login == sentUser.Login {
			notFound := bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(sentUser.Password))
			if notFound == nil {
				return user, nil
			}
		}
	}
	return emptyUser, errors.New("user not found")
}
