package auth

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// DataHandler has the db connection
type DataHandler struct {
	db *gorm.DB
}

// NewDataHandler init a DataHandler and returns a pointer to it
func NewDataHandler() *DataHandler {
	db, err := gorm.Open("sqlite3", "./data/users.db")
	if err != nil {
		panic("failed to connect database")
	}
	db.LogMode(true)

	// Migrate the schema
	db.AutoMigrate(&User{})
	return &DataHandler{db: db}
}

func (d *DataHandler) createUser(user User) error {
	d.db.Create(&user)
	return nil
}

func (d *DataHandler) saveUser(user User) error {
	d.db.Save(&user)
	return nil
}

func (d *DataHandler) getUsers() []User {
	var users []User
	d.db.Find(&users)
	return users
}

func (d *DataHandler) getUser(id int) User {
	var user User
	d.db.First(&user, id)
	return user
}

func (d *DataHandler) removeUser(id int) error {
	var o User
	if err := d.db.First(&o, id).Error; err != nil {
		return errors.New("id does not exist")
	}
	d.db.Delete(&o)
	return nil
}
