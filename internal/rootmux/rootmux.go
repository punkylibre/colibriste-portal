package rootmux

import (
	"context"
	"errors"
	"net/http"
	"os"
	"time"

	"github.com/etherlabsio/healthcheck/v2"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"gitlab.com/punkylibre/colibriste-portal/internal/auth"
	"gitlab.com/punkylibre/colibriste-portal/internal/models"
	"gitlab.com/punkylibre/colibriste-portal/pkg/common"
	"gitlab.com/punkylibre/colibriste-portal/pkg/middlewares"
)

// RootMux represents the main controller of the application
type RootMux struct {
	Mux     http.Handler
	Manager *auth.Manager
}

// CreateRootMux creates a RootMux
func CreateRootMux(port int, staticDir string) RootMux {
	hostname := os.Getenv("HOSTNAME")
	// Create the main handler
	mainMux := http.NewServeMux()

	// Authentication endpoints
	m := auth.NewManager()
	mainMux.HandleFunc("/OAuth2Login", m.HandleOAuth2Login)
	mainMux.Handle("/OAuth2Callback", m.HandleOAuth2Callback())
	mainMux.HandleFunc("/Logout", m.HandleLogout)
	mainMux.HandleFunc("/Login", m.HandleInMemoryLogin)

	// Colibriste API endpoints
	dh := models.NewDataHandler()
	mainMux.HandleFunc("/api/", dh.ProcessAPI)

	// Common API endpoints
	commonMux := http.NewServeMux()
	mainMux.Handle("/api/common/WhoAmI", auth.ValidateAuthMiddleware(auth.WhoAmI(), []string{"*"}, false))
	commonMux.HandleFunc("/Share", auth.GetShareToken)
	mainMux.Handle("/api/common/", http.StripPrefix("/api/common", auth.ValidateAuthMiddleware(commonMux, []string{"*"}, true)))

	// ADMIN API ENDPOINTS
	adminMux := http.NewServeMux()
	adminMux.HandleFunc("/users/", auth.ProcessUsers)
	mainMux.Handle("/api/admin/", http.StripPrefix("/api/admin", auth.ValidateAuthMiddleware(adminMux, []string{"admin"}, true)))

	// Serve static files falling back to serving index.html
	mainMux.Handle("/", middlewares.NoCache(http.FileServer(&common.FallBackWrapper{Assets: http.Dir(staticDir)})))

	// Healthchck API

	db, _ := gorm.Open("sqlite3", "./data/test.db")
	mainMux.Handle("/healthcheck", healthcheck.Handler(
		// WithTimeout allows you to set a max overall timeout.
		healthcheck.WithTimeout(5*time.Second),

		healthcheck.WithChecker(
			"database", healthcheck.CheckerFunc(
				func(ctx context.Context) error {
					return db.DB().PingContext(ctx)
				},
			),
		),

		healthcheck.WithChecker(
			"database", healthcheck.CheckerFunc(
				func(ctx context.Context) error {
					var o []models.Application
					db.Find(&o)
					if len(o) < 2 {
						return errors.New("No app founds")
					}
					return nil
				},
			),
		),
	))

	// Put it together into the main handler
	mux := http.NewServeMux()
	mux.Handle(hostname+"/", middlewares.WebSecurity(mainMux, "*."+hostname+":*", false))
	return RootMux{mux, &m}
}
