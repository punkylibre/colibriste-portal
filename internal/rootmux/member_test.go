package rootmux

import (
	"encoding/json"
	"testing"

	"gitlab.com/punkylibre/colibriste-portal/internal/auth"
)

/**
CLIENT TESTS (this tests are to check that a normally logged user can access the apps that is allowed to and only that)
**/
func MemberTests(t *testing.T) {
	// Create the tester
	ts, do, _ := createTester(t)
	defer ts.Close() // Close the tester
	tests := func() {
		// Get the XSRF Token
		response := do("GET", "/api/common/WhoAmI", noH, "", 200, "")
		token := auth.TokenData{}
		json.Unmarshal([]byte(response), &token)
		xsrfHeader := map[string]string{"XSRF-TOKEN": token.XSRFToken}

		// Create an app should fail
		do("POST", "/api/Application", xsrfHeader, `{"Name":"App4","Order":1,"Description":"Desc de l'app4","Group":"admin","Url":"https://exemple-admin-other.com","FaIcon":"fa-trash"}`, 405, `method not allowed`)
		// Get an applications
		do("GET", "/api/Application/1", xsrfHeader, "", 405, `method not allowed`)
		// Try to gel all apps
		do("GET", "/api/Application/", xsrfHeader, "", 200, `[{"ID":2,"Order":1,"Name":"App2","Description":"Desc de l'app2","Group":"member","Url":"https://exemple-member.com","FaIcon":"fa-minus"},{"ID":3,"Order":1,"Name":"App3","Description":"Desc de l'app3","Group":"other","Url":"https://exemple.com","FaIcon":"fa-plus"}]`)
		// Update an app should suceed
		do("PUT", "/api/Application/1", xsrfHeader, `{"Name":"App5","Order":1,"Description":"Desc de l'app1","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-plus"}`, 405, `method not allowed`)
		// Delete an app should fail with 405
		do("DELETE", "/api/Application/1", xsrfHeader, ``, 405, `method not allowed`)

	}
	// Do a in memory login with an known user
	do("POST", "/Login", noH, `{"login": "Dupond","password": "password"}`, 200, "")
	// Run the tests
	tests()
	// Try to logout (must pass)
	do("GET", "/Logout", noH, "", 200, "Logout OK")

}
