package rootmux

import (
	"net/http"
	"testing"
)

/**
UNLOGGED USER TESTS (this tests are to check that the security protections works)
**/

func UnLoggedUserTests(t *testing.T) {
	// Create the tester
	ts, do, _ := createTester(t)
	defer ts.Close() // Close the tester
	// Try to access the main page (must pass)
	do("GET", "/", noH, "", 200, "<!DOCTYPE html>")
	// Try to get the user informations (must fail)
	do("GET", "/api/common/WhoAmI", noH, "", 401, "error extracting token")
	// Do a in memory login with an unknown user
	do("POST", "/Login", noH, `{"login": "unknownuser","password": "password"}`, http.StatusForbidden, `user not found`)
	// Do a in memory login with a known user but bad password
	do("POST", "/Login", noH, `{"login": "admin","password": "badpassword"}`, http.StatusForbidden, `user not found`)

	// Create an app should fail
	do("POST", "/api/Application", noH, `{"Name":"App4","Order":1,"Description":"Desc de l'app4","Group":"admin","Url":"https://exemple-admin-other.com","FaIcon":"fa-trash"}`, 405, `method not allowed`)
	// Get an applications
	do("GET", "/api/Application/1", noH, "", 405, `method not allowed`)
	// Try to gel all apps
	do("GET", "/api/Application/", noH, "", 200, `[{"ID":3,"Order":1,"Name":"App3","Description":"Desc de l'app3","Group":"other","Url":"https://exemple.com","FaIcon":"fa-plus"}]`)
	// Update an app should suceed
	do("PUT", "/api/Application/1", noH, `{"Name":"App5","Order":1,"Description":"Desc de l'app1","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-plus"}`, 405, `method not allowed`)
	// Delete an app should fail with 405
	do("DELETE", "/api/Application/1", noH, ``, 405, `method not allowed`)

}
