package rootmux

import (
	"encoding/json"
	"testing"

	"gitlab.com/punkylibre/colibriste-portal/internal/auth"
)

/**
ADMIN TESTS (those tests are to check the admins authorization)
**/
func AdminTests(t *testing.T) {
	// Create the tester
	ts, do, _ := createTester(t)
	defer ts.Close() // Close the tester
	tests := func() {
		// Get the XSRF Token
		response := do("GET", "/api/common/WhoAmI", noH, "", 200, "")
		token := auth.TokenData{}
		json.Unmarshal([]byte(response), &token)
		xsrfHeader := map[string]string{"XSRF-TOKEN": token.XSRFToken}

		// Create an app should suceed
		do("POST", "/api/Application", xsrfHeader, `{"Name":"App4","Order":1,"Description":"Desc de l'app4","Group":"admin","Url":"https://exemple-admin-other.com","FaIcon":"fa-trash"}`, 200, ``)
		// Get an applications
		do("GET", "/api/Application/1", xsrfHeader, "", 200, `{"ID":1,"Order":1,"Name":"App1","Description":"Desc de l'app1","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-plus"}`)
		// Try to gel all apps
		do("GET", "/api/Application/", xsrfHeader, "", 200, `[{"ID":1,"Order":1,"Name":"App1","Description":"Desc de l'app1","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-plus"},{"ID":4,"Order":1,"Name":"App4","Description":"Desc de l'app4","Group":"admin","Url":"https://exemple-admin-other.com","FaIcon":"fa-trash"},{"ID":2,"Order":1,"Name":"App2","Description":"Desc de l'app2","Group":"member","Url":"https://exemple-member.com","FaIcon":"fa-minus"},{"ID":3,"Order":1,"Name":"App3","Description":"Desc de l'app3","Group":"other","Url":"https://exemple.com","FaIcon":"fa-plus"}]`)
		// Update an app should suceed
		do("PUT", "/api/Application/1", xsrfHeader, `{"Name":"App5","Order":1,"Description":"Desc de l'app1","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-plus"}`, 200, `{"ID":1,"Order":1,"Name":"App5","Description":"Desc de l'app1","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-plus"}`)
		// Try to delete an app
		do("DELETE", "/api/Application/4", xsrfHeader, ``, 200, ``)

	}
	// Do an OAuth2 login with an known admin
	do("GET", "/OAuth2Login", noH, "", 200, "<!DOCTYPE html>")
	tests()
	// Try to logout (must pass)
	do("GET", "/Logout", noH, "", 200, "Logout OK")

}
