package rootmux

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"net/url"
	"os"
	"regexp"
	"testing"

	"gitlab.com/punkylibre/colibriste-portal/internal/auth"
	"gitlab.com/punkylibre/colibriste-portal/pkg/tester"
	"gitlab.com/punkylibre/colibriste-portal/pkg/tokens"

	"gitlab.com/punkylibre/colibriste-portal/internal/mocks"
)

var (
	reg, _              = regexp.Compile("[\n \t]+")
	initialUsersBuff, _ = ioutil.ReadFile("../../configs/users.json")
	initialUsers        = reg.ReplaceAllString(string(initialUsersBuff), "")
	newUser             = `{"id":"3","login":"new_user","memberOf":["USERS"],"password":"test"}`
	noH                 = map[string]string{}
)

func init() {
	tokens.Init("../../configs/tokenskey.json", true)
}

func TestAll(t *testing.T) {
	os.Mkdir("data", os.ModePerm)
	os.Create("./data/test.db")
	os.Link("../../data/users.db", "./data/users.db")
	// Create the mock OAuth2 server
	oAuth2Server := httptest.NewServer(mocks.CreateMockOAuth2())
	defer oAuth2Server.Close()
	// Create the mock API server
	go http.ListenAndServe(":8091", mocks.CreateMockAPI())
	// Set the constants with environment variables
	os.Setenv("HOSTNAME", "colibriste.io")
	os.Setenv("ADMIN_GROUP", "ADMIN")
	os.Setenv("MEMBER_GROUP", "MEMBER")
	os.Setenv("CLIENT_ID", "clientid")
	os.Setenv("CLIENT_SECRET", "clientsecret")
	os.Setenv("TOKEN_URL", oAuth2Server.URL+"/token")
	os.Setenv("USERINFO_URL", oAuth2Server.URL+"/userinfo")
	os.Setenv("LOGOUT_URL", oAuth2Server.URL+"/logout")
	// Set up testers
	os.Setenv("AUTH_URL", oAuth2Server.URL+"/auth-wrong-state") // Set the server to access failing OAuth2 endpoints
	Oauth2Tests(t)
	os.Setenv("AUTH_URL", oAuth2Server.URL+"/auth") // Set the server to access the correct OAuth2Endpoint
	os.Setenv("USERINFO_URL", oAuth2Server.URL+"/admininfo")

	resetData(t)
	UnLoggedUserTests(t)
	resetData(t)
	MemberTests(t)
	resetData(t)
	AdminTests(t)
	resetData(t)
	appTests(t)

	os.RemoveAll("./data")
}

/**
SECURITY TESTS (this tests are to check that the security protections works)
**/
func Oauth2Tests(t *testing.T) {
	// Create the tester
	ts, do, _ := createTester(t)
	defer ts.Close() // Close the tester
	// Try to login (must fail)
	do("GET", "/OAuth2Login", noH, "", 500, "invalid oauth state")
}

func appTests(t *testing.T) {

	ts, do, _ := createTester(t)
	defer ts.Close() // Close the tester
	tests := func() {
		// Get the XSRF Token
		response := do("GET", "/api/common/WhoAmI", noH, "", 200, "")
		token := auth.TokenData{}
		json.Unmarshal([]byte(response), &token)
		xsrfHeader := map[string]string{"XSRF-TOKEN": token.XSRFToken}

		// Create an app should fail because of duplicated name
		do("POST", "/api/Application", xsrfHeader, `{"Name":"App1","Order":1,"Description":"Desc de l'app4","Group":"admin","Url":"https://exemple-admin-other.com","FaIcon":"fa-trash"}`, 409, `Name Already exist.`)
		// Create an app should fail because of duplicated url
		do("POST", "/api/Application", xsrfHeader, `{"Name":"App4","Order":1,"Description":"Desc de l'app4","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-trash"}`, 409, `URL Already exist.`)
		// Create an app should fail because group don't exist
		do("POST", "/api/Application", xsrfHeader, `{"Name":"App4","Order":1,"Description":"Desc de l'app4","Group":"group","Url":"https://exemple-admin.com","FaIcon":"fa-trash"}`, 409, `URL Already exist.`)

		// Update an app should fail because of duplicated name
		do("PUT", "/api/Application/1", xsrfHeader, `{"Name":"App2","Order":1,"Description":"Desc de l'app1","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-plus"}`, 409, `Name Already exist.`)
		// Update an app should fail because of duplicated url
		do("PUT", "/api/Application/1", xsrfHeader, `{"Name":"App1","Order":1,"Description":"Desc de l'app1","Group":"admin","Url":"https://exemple.com","FaIcon":"fa-plus"}`, 409, `URL Already exist.`)
		// Update an app should fail because group don't exist
		do("PUT", "/api/Application/1", xsrfHeader, `{"Name":"App1","Order":1,"Description":"Desc de l'app1","Group":"group","Url":"https://exemple-admin.com","FaIcon":"fa-plus"}`, 409, `Group don't exist.`)

	}
	// Do an OAuth2 login with an known admin
	do("GET", "/OAuth2Login", noH, "", 200, "<!DOCTYPE html>")
	tests()
	// Try to logout (must pass)
	do("GET", "/Logout", noH, "", 200, "Logout OK")

}

func resetData(t *testing.T) {
	os.RemoveAll("./data")
	os.Mkdir("data", os.ModePerm)
	os.Create("./data/test.db")
	os.Link("../../data/users.db", "./data/users.db")

	ts, do, _ := createTester(t)
	defer ts.Close() // Close the tester

	// Init Data in DB tests
	init := func() {
		response := do("GET", "/api/common/WhoAmI", noH, "", 200, "")
		token := auth.TokenData{}
		json.Unmarshal([]byte(response), &token)
		xsrfHeader := map[string]string{"XSRF-TOKEN": token.XSRFToken}

		do("POST", "/api/Application", xsrfHeader, `{"Name":"App1","Order":1,"Description":"Desc de l'app1","Group":"admin","Url":"https://exemple-admin.com","FaIcon":"fa-plus"}`, 200, ``)
		do("POST", "/api/Application", xsrfHeader, `{"Name":"App2","Order":1,"Description":"Desc de l'app2","Group":"member","Url":"https://exemple-member.com","FaIcon":"fa-minus"}`, 200, ``)
		do("POST", "/api/Application", xsrfHeader, `{"Name":"App3","Order":1,"Description":"Desc de l'app3","Group":"other","Url":"https://exemple.com","FaIcon":"fa-plus"}`, 200, ``)

	}
	do("POST", "/Login", noH, `{"login": "admin","password": "password"}`, 200, "")
	init()
	do("GET", "/Logout", noH, "", 200, "Logout OK")
}

func createTester(t *testing.T) (*httptest.Server, func(method string, url string, headers map[string]string, payload string, expectedStatus int, expectedBody string) string, func(method string, url string, headers map[string]string, payload string, expectedStatus int, expectedBody string) string) {
	// Create the server
	mux := CreateRootMux(1443, "../../web")
	ts := httptest.NewServer(mux.Mux)
	url, _ := url.Parse(ts.URL)
	port := url.Port()
	mux.Manager.Config.RedirectURL = "http://" + os.Getenv("HOSTNAME") + ":" + port + "/OAuth2Callback"
	mux.Manager.Hostname = "http://" + os.Getenv("HOSTNAME") + ":" + port
	// Create the cookie jar
	jar, _ := cookiejar.New(nil)
	// wrap the testing function
	return ts, tester.CreateServerTester(t, port, os.Getenv("HOSTNAME"), jar), tester.CreateServerTester(t, port, os.Getenv("HOSTNAME"), nil)
}
