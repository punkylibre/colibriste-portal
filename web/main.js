import * as Home from '/components/home/home.js';
import * as Navbar from '/components/navbar/navbar.js';
import * as Applications from '/components/application/application-page.js';
import * as LegalMentions from '/components/legal-mentions/legal-mentions.js';
import { AnimateCSS } from '/services/common/common.js';

const mountPoint = document.getElementById('main');
const spinner = document.getElementById('spinner');
let sysInfoInterval;

document.addEventListener('DOMContentLoaded', function () {
  Navbar.mount('navbar');
  window.addEventListener('hashchange', navigate);
  navigate();
});

async function navigate() {
  clearInterval(sysInfoInterval);
  switch (location.hash) {
    case '#home':
      load(mountPoint, async function () {
        await Home.mount('main');
      });
      break;
    case '#applications':
      load(mountPoint, async function () {
        await Applications.mount('main');
      });
      break;
    case '#legal-mentions':
      load(mountPoint, async function () {
        await LegalMentions.mount('main');
      });
      break;
    default:
      location.hash = '#home';
      break;
  }
}

async function load(element, domAlteration) {
  AnimateCSS(element, 'fadeOut', async function () {
    element.classList.add('is-hidden');
    spinner.classList.remove('is-hidden');
    AnimateCSS(spinner, 'fadeIn');
    if (typeof domAlteration === 'function') {
      await domAlteration();
      AnimateCSS(spinner, 'fadeOut', function () {
        spinner.classList.add('is-hidden');
      });
      element.classList.remove('is-hidden');
      AnimateCSS(element, 'fadeIn');
    }
  });
}
