// Imports
import * as brand from '/assets/brand/brand.js';
import * as Auth from '/services/auth/auth.js';

// local variables
let user;
let menu;

export async function mount(mountpoint) {
  const where = document.getElementById(mountpoint);
  window.document.title = brand.windowTitle;
  window.addEventListener('hashchange', CreateMenu);
  where.innerHTML = /* HTML */ `
    <div id="navbar-burger">
      <div id="navbar-start">
        <a class="navbar-item" href="/">${brand.navTitle}</a>
      </div>
      <div id="burger">
        <hr />
        <hr />
        <hr />
      </div>
    </div>
    <div id="navbar-menu" class="is-hidden"></div>
  `;

  menu = document.getElementById('navbar-menu');
  // Hamburger menu
  const burger = document.getElementById('burger');
  const openClose = (e) => {
    console.log(e);
    if (menu.classList.contains('is-hidden')) {
      menu.classList.remove('is-hidden');
    } else {
      menu.classList.add('is-hidden');
    }
  };
  burger.addEventListener('click', openClose);
  menu.addEventListener('click', openClose);

  CreateMenu();
}

export async function CreateMenu() {
  user = await Auth.GetUser();
  menu.innerHTML = /* HTML */ `
    <div id="navbar-start">
      <a class="navbar-item" href="/">${brand.navTitle}</a>
    </div>
    <div id="navbar-center">
      ${user === undefined
        ? /* HTML */ `
            <a class="navbar-item ${location.hash === '#home' ? 'active' : ''}" href="#home"
              ><i class="navbar-menu-icon fas fa-home"></i>Accueil</a
            >
            <a class="navbar-item ${location.hash === '#legal-mentions' ? 'active' : ''}" href="#legal-mentions"
              ><i class="navbar-menu-icon fas fa-balance-scale"></i>Mentions Légales</a
            >
          `
        : /* HTML */ `
            <a class="navbar-item ${location.hash === '#home' ? 'active' : ''}" href="#home"
              ><i class="navbar-menu-icon fas fa-home"></i>Accueil</a
            >
            <a class="navbar-item ${location.hash === '#legal-mentions' ? 'active' : ''}" href="#legal-mentions"
              ><i class="navbar-menu-icon fas fa-balance-scale"></i>Mentions Légales</a
            >
            ${user.role == 'ADMIN'
              ? /* HTML */ `
                  <a class="navbar-item ${location.hash === '#applications' ? 'active' : ''}" href="#applications"
                    ><i class="navbar-menu-icon fas fa-list-alt"></i>Applications</a
                  >
                `
              : ''}
          `}
    </div>
    <div id="navbar-end">
      ${user === undefined
        ? /* HTML */ `
            <a class="navbar-item" href="/OAuth2Login"><i class="navbar-menu-icon fas fa-sign-in-alt"></i>Connexion</a>
          `
        : /* HTML */ `
            <a class="navbar-item" href="/Logout"><i class="navbar-menu-icon fas fa-sign-out-alt"></i>Déconnexion</a>
          `}
    </div>
  `;
}
