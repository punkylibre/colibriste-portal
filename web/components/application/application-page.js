// Imports
import { RandomString } from '/services/common/common.js';
import * as Auth from '/services/auth/auth.js';
import * as AppList from '/components/application/app-list.js';
import * as AppForm from '/components/application/app-form.js';

// DOM elements

// local variables
let current_user;

export async function mount(where) {
  const applicationPage = new ApplicationPage();
  await applicationPage.mount(where);
}

class ApplicationPage {
  constructor() {
    // Random id seed
    this.prefix = RandomString(8);
    this.currentApp = null;
  }

  async mount(where) {
    const appPage = this;
    document.getElementById(where).innerHTML = /* HTML */ ` <div id="columns-app-administration">
      <div id="app-list-component" class="column"></div>
      <div id="app-form-component" class="column"></div>
    </div>`;
    current_user = await Auth.GetUser();
    this.appList = await AppList.mount('app-list-component', this);
    this.appForm = await AppForm.mount('app-form-component', this);
  }

  reloadList() {
    this.appList.displayApps();
  }

  selectApp(app) {
    this.appForm.app = app;
    this.appForm.mount();
  }
}
