// Imports
import * as Auth from '/services/auth/auth.js';
import * as Common from '/services/common/common.js';

// DOM elements

// local variables
let current_user;

export async function mount(where, parent) {
  const appList = new AppList(parent);
  await appList.mount(where);
  await appList.displayApps();
  return appList;
}

class AppList {
  constructor(parent) {
    this.parent = parent;
  }

  async mount(where) {
    current_user = await Auth.GetUser();

    document.getElementById(where).innerHTML = /* HTML */ `
      <h1>
        Liste des applications <span class="icon" id="add-btn"> <i class="fas fa-plus"></i> </span>
      </h1>
      <div id="app-list"></div>
    `;

    this.handleDom();
  }

  handleDom() {
    const appList = this;

    document.getElementById(`add-btn`).addEventListener('click', async function () {
      appList.parent.selectApp(null);
    });
  }

  async displayApps() {
    let applications = [];
    try {
      const response = await fetch('/api/Application/', {
        method: 'GET',
        headers: new Headers({
          'XSRF-Token': current_user.xsrftoken,
        }),
      });
      if (response.status !== 200) {
        throw new Error(`Applications could not be fetched (status ${response.status})`);
      }
      applications = await response.json();
    } catch (e) {
      Messages.Show('is-warning', e.message);
      console.error(e);
    }

    const appList = this;
    const markup = applications.map((app) => this.appTemplate(app)).join('');
    document.getElementById('app-list').innerHTML = markup;
    applications.map((app) => {
      document.getElementById(`apps-app-${app.ID}`).addEventListener('click', function () {
        appList.parent.selectApp(app);
      });
    });
  }

  appTemplate(app) {
    return /* HTML */ `
      <div id="apps-app-${app.ID}" class="app">
        <div>
          <h2>${app.Name}</h2>
          <p>${Common.getGroupString(app.Group)}</p>
        </div>
        <div class="icon">
          <i class="fas ${app.FaIcon} fa-2x"></i>
        </div>
      </div>
    `;
  }
}
