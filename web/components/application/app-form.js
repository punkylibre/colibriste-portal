// Imports
import * as Auth from '/services/auth/auth.js';
import * as Messages from '/services/messages/messages.js';

// DOM elements

// local variables

export async function mount(where, parent) {
  const appForm = new AppForm(where, parent);
  await appForm.mount();
  return appForm;
}

class AppForm {
  constructor(where, parent) {
    this.where = where;
    this.parent = parent;
  }

  async mount() {
    this.current_user = await Auth.GetUser();
    document.getElementById(this.where).innerHTML = /* HTML */ `
      <h1>${
        this.app
          ? 'Modifier ' + this.app.Name + '<span class="icon" id="delete-btn"> <i class="fas fa-trash"></i> </span>'
          : 'Ajouter une application'
      }</h2>
      <form>
        <div class="field">
          <label>Nom : </label><br />
          <div class="control">
            <input type="text" name="name" id="name-field" style="width: 50%" value="${this.app ? this.app.Name : ''}" />
          </div>
        </div>
        <div class="field">
          <label>Url : </label><br />
          <div class="control">
            <input type="text" name="url" id="url-field" style="width: 50%" value="${this.app ? this.app.Url : ''}" />
          </div>
        </div>
        <div class="field">
          <label>Groupe : </label><br />
          <div class="control">
            <select name="group-select" id="group-select">
              <option value="admin">Administrateur</option>
              <option value="member">Membre</option>
              <option value="other">Public</option>
            </select>
          </div>
        </div>
        <div class="field">
          <label>Fa-icon : </label><br />
          <div class="control">
            <input type="text" name="icon" id="icon-field" value="${this.app ? this.app.FaIcon : ''}" />
          </div>
        </div>
        <div class="field">
          <label>Ordre : </label><br />
          <div class="control">
            <input type="number" name="order" id="order-field" style="width: 10%" value="${this.app ? this.app.Order : ''}" min="1" />
          </div>
        </div>
        <div class="field">
          <label>Description : </label><br />
          <div class="control">
            <textarea name="description" id="description-field" placeholder="Description" style="width: 98%"></textarea>
          </div>
        </div>
      </form>
      <div class="buttons" id="buttons-form-app">
        <button class="button" id="cancel-btn">
          <span class="icon"> <i class="fas fa-times"></i> </span><span>Annuler</span>
        </button>
        <button class="button is-primary" id="validate-btn">
          <span class="icon"> <i class="fas fa-check"></i> </span><span>${this.app ? 'Modifier' : 'Ajouter'}</span>
        </button>
      </div>
    `;

    if (this.app) {
      document.getElementById('description-field').value = this.app.Description;
      document.getElementById('group-select').value = this.app.Group;
    }

    this.handleDom();
  }

  handleDom() {
    const appForm = this;
    document.getElementById(`cancel-btn`).addEventListener('click', async function () {
      appForm.app = null;
      appForm.mount(appForm.where);
    });

    document.getElementById(`validate-btn`).addEventListener('click', async function () {
      appForm.app = await appForm.saveApp();
      appForm.mount(appForm.where);
      appForm.parent.reloadList();
    });

    if (this.app) {
      document.getElementById(`delete-btn`).addEventListener('click', async function () {
        appForm.app = await appForm.deleteApp();
        appForm.mount(appForm.where);
        appForm.parent.reloadList();
      });
    }
  }

  async saveApp() {
    let method;
    let ID;
    if (this.app) {
      method = 'PUT';
      ID = this.app.ID;
    } else {
      method = 'POST';
      ID = null;
    }

    try {
      const response = await fetch('/api/Application/' + ID, {
        method: method,
        headers: new Headers({
          'XSRF-Token': this.current_user.xsrftoken,
        }),
        body: JSON.stringify({
          ID: ID,
          Name: document.getElementById('name-field').value,
          Url: document.getElementById('url-field').value,
          Group: document.getElementById('group-select').value,
          FaIcon: document.getElementById('icon-field').value,
          Order: parseInt(document.getElementById('order-field').value),
          Description: document.getElementById('description-field').value,
        }),
      });
      if (response.status !== 200) {
        throw new Error(`Application could not be updated or created (status ${response.status})`);
      }
      return await response.json();
    } catch (e) {
      Messages.Show('is-warning', e.message);
      console.error(e);
      return;
    }
  }

  async deleteApp() {
    try {
      const response = await fetch('/api/Application/' + this.app.ID, {
        method: 'delete',
        headers: new Headers({
          'XSRF-Token': this.current_user.xsrftoken,
        }),
      });
      if (response.status !== 200) {
        throw new Error(`Application could not be deleted (status ${response.status})`);
      }
    } catch (e) {
      Messages.Show('is-warning', e.message);
      console.error(e);
    }
  }
}
