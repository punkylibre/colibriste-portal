// Imports
import * as Messages from '/services/messages/messages.js';
import * as Auth from '/services/auth/auth.js';

// DOM elements

// local variables
let current_user;
let applications = [];

export async function mount(where) {
  const homePage = new HomePage();
  await homePage.mount(where);
  await homePage.mountApps();
}

class HomePage {
  constructor() {}

  async mount(where) {
    document.getElementById(where).innerHTML = /* HTML */ `
      <div>
        <div id="public-apps" class="app-list"></div>
        <div id="member-apps" class="app-list"></div>
        <div id="admin-apps" class="app-list"></div>
      </div>
    `;
    current_user = await Auth.GetUser();
    try {
      const response = await fetch('/api/Application/', {
        method: 'GET',
        headers: new Headers({
          'XSRF-Token': current_user?.xsrftoken,
        }),
      });
      if (response.status !== 200) {
        throw new Error(`Applications could not be fetched (status ${response.status})`);
      }
      applications = await response.json();
    } catch (e) {
      Messages.Show('is-warning', e.message);
      console.error(e);
    }
  }

  async mountApps() {
    this.mountPublicApps();
    this.mountMemberApps();
    this.mountAdminApps();
  }

  mountPublicApps() {
    let publicApp = applications.filter((app) => {
      return app.Group === 'other';
    });
    if (publicApp.length > 0) {
      const markup = publicApp.map((application) => this.appTemplate(application)).join('');
      document.getElementById('public-apps').innerHTML = markup;

      let title = document.createElement('h1');
      title.innerHTML = /* HTML */ `Applications tout public`;
      title.className = 'apps-title';
      document.getElementById('public-apps').before(title);
    }
  }

  mountMemberApps() {
    let memberApp = applications.filter((app) => {
      return app.Group === 'member';
    });
    if (memberApp.length > 0) {
      const markup = memberApp.map((application) => this.appTemplate(application)).join('');
      document.getElementById('member-apps').innerHTML = markup;

      let title = document.createElement('h1');
      title.innerHTML = /* HTML */ `Applications membres`;
      title.className = 'apps-title';
      document.getElementById('member-apps').before(title);
    }
  }

  mountAdminApps() {
    let adminApp = applications.filter((app) => {
      return app.Group === 'admin';
    });
    if (adminApp.length > 0) {
      const markup = adminApp.map((application) => this.appTemplate(application)).join('');
      document.getElementById('admin-apps').innerHTML = markup;

      let title = document.createElement('h1');
      title.innerHTML = /* HTML */ `Applications administrateur`;
      title.className = 'apps-title';
      document.getElementById('admin-apps').before(title);
    }
  }

  appTemplate(app) {
    return /* HTML */ `
      <div id="public-app-${app.ID}" class="app">
        <div class="icon">
          <i class="fas ${app.FaIcon}"></i>
        </div>
        <h3>${app.Name}</h3>
        <p>${app.Description}</p>
        <a href="${app.Url}" class="link-app" target="_blank" rel="noopener noreferrer">Ouvrir</a>
      </div>
    `;
  }
}
