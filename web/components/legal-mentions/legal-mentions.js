// Imports

// DOM elements

// local variables

export async function mount(where) {
  const legalMentionsPage = new LegalMentionsPage();
  await legalMentionsPage.mount(where);
}

class LegalMentionsPage {
  constructor() {}

  async mount(where) {
    document.getElementById(where).innerHTML = /* HTML */ `
      <div>
        <div id="legal-mentions" class="content">
          <h1>Mentions légales</h1>
          <blockquote>Dernière mise à jour le 11/11/2022</blockquote>

          <h2>Informations générales</h2>
          <p>
            Colibriste est une plateforme de services libres et ouverts s'inspirant de la démarche des
            <a target="_blank" href="https://www.chatons.org/node/1">CHATONS</a>, afin que les internautes puissent
            accèder à des services en ligne respectant leur vie privée.
          </p>
          <p>
            Certains services sont accessibles au grand public (sans comptes) directement depuis le portail de
            <a target="_blank" href="https://colibriste.org">Colibriste</a> . D'autres nécessitent la création d'un
            compte. Pour cela contacter l'adresse mail
            <a href="mailto:admin@colibriste.org">admin@colibriste.org</a> afin de rejoindre la plateforme. Un compte
            Colibriste vous permet d'accéder à différents services avec un seul et unique compte via SSO: portail,
            cloud, documentation. D'autres services nécessite des comptes par service : password, rss, mail.
          </p>
          <p>La plateforme est gérée bénévolement par ses membres.</p>
          <ul>
            <li>
              Animation éditoriale, gestion et mise à jour : équipe admin de Colibriste
              <a href="mailto:admin@colibriste.org">admin@colibriste.org</a>
            </li>
            <li>
              Maintenance technique : équipe admin de Colibriste
              <a href="mailto:admin@colibriste.org">admin@colibriste.org</a>
            </li>
            <li>Hébergement : OVH sur le Data Center de Gravelines.</li>
            <li>Stockage des sauvegardes : Domicil personnel en France sur disque dur.</li>
          </ul>

          <p>
            Ce site est optimisé pour Mozilla Firefox (version 105 et supérieures), Google Chrome (version 105 et
            supérieures). Ce site n'a pas été testé sur Safari (iOS et iPhone)
          </p>

          <h2>Connexion et SSO (Single Sign On)</h2>
          <p>
            Les données personnelles collectées sur la plateforme Colibriste, peuvent impliquer une connexion préalable
            à un compte Connect Colibriste. Ce compte permet aux utilisateurs d’accéder aux services proposés par
            Colibriste à partir d’un identifiant unique et individuel. Les données personnelles collectées par
            Colibriste, via un compte Connect Colibriste ou directement saisies dans les formulaires des services,
            résultent d’un renseignement volontaire par les utilisateurs et ne concernent que les demandes relatives aux
            services en lignes proposés. Ces données sont conservées le temps de l'activité des comptes sur les
            différents services.
          </p>
          <p>
            Les données personnelles collectées par Colibriste dans le cadre du compte Colibriste résultent d’un
            renseignement volontaire par les utilisateurs et se limitent aux données du profil du compte. Ces données ne
            sont partagées avec les autres services de Colibriste qu’avec le consentement exprès et préalable de la
            personne concernée lors de l’accès aux différents services.
          </p>

          <h2>Droits d'accès de modification et de rectification</h2>
          <p>
            Traitement des données personnelles et droit d'accès, de modification et de suppression. <br />
            Conformément à la loi 78-17 du 6 janvier 1978 modifiée relative à l'information, aux fichiers et aux
            libertés, vous disposez à tout moment d'un droit d'accès, de rectification, d'opposition au traitement et de
            retrait de consentement d’utilisation de vos données à caractère personnel, sur la plateforme Colibriste,
            vous pouvez vous adresser à : <a href="mailto:admin@colibriste.org">admin@colibriste.org</a>
          </p>
          <p>
            La plateforme Colibriste est une plateforme gérée de manière bénévole, nous ferons de notre mieux pour
            traiter votre demande dans les plus brefs délais, mais cela peut prendre un peu de temps.
          </p>
          <p>
            En cas de désaccord persistant concernant vos données, vous avez le droit de saisir la CNIL à l’adresse
            suivante : Commission Nationale Informatique et Libertés, 3 place de Fontenoy 75007 Paris, 01 53 73 22 22,
            <a target="_blank" href="https://www.cnil.fr/fr/vous-souhaitez-contacter-la-cnil">CNIL</a>
          </p>

          <h2>Analyse d'audiences</h2>
          <p>
            Aucune analyse d'audience n'est faite sur les différents services de Colibriste que ce soit de façon anonyme
            ou ciblée. Toutefois pour des raisons de maintenance et d'amélioration de la fiabilité techniques, les logs
            réseaux sont enregistrés ce qui inclue l'enregistrement de votre addresse IP lors de la navigation sur les
            services Colibriste.
          </p>

          <h2>Cookies</h2>
          <p>Cookies stockant des informations relatives à la navigation d'un visiteur.</p>
          <p>
            Lors de votre visite sur les différents services, ceux-ci peuvent enregistrer des cookies sur votre
            ordinateur. Ils stockent des informations relatives à votre navigation et indispensables au fonctionnement
            des services (authentification, etc). La durée de conservation de ces informations varient selon chaque
            service. Vous pouvez vous opposer à l'enregistrement des cookies en paramétrant votre navigateur de la
            manière suivante :
          </p>
          <ul>
            <li>
              <a
                target="_blank"
                href="https://support.mozilla.org/fr/kb/protection-renforcee-contre-pistage-firefox-ordinateur?redirectslug=activer-desactiver-cookies&redirectlocale=fr"
                >Mozilla Firefox</a
              >
            </li>
            <li>
              <a target="_blank" href="https://support.google.com/chrome/answer/95647?hl=fr"
                >Google Chrome pour PC de bureau</a
              >
            </li>
            <li>
              <a target="_blank" href="https://support.google.com/chrome/answer/2392709?hl=fr"
                >Google Chrome pour mobile</a
              >
            </li>
            <li>Ou en utilisant la navigation privée de votre navigateur</li>
          </ul>

          <h2>Droit des bases de données</h2>
          <p>
            Les bases de données sont protégées par la loi du 1er juillet 1998 et le régime français du droit d'auteur.
          </p>

          <h2>Support</h2>
          <p>
            L'utilisateur dispose d’une aide en ligne pour lui faciliter l’utilisation des services Colibriste. Cette
            fonction est mise œuvre via la
            <a target="_blank" href="https://doc.colibriste.org/">documentation Colibriste</a>. Pour toute question
            supplémentaire veuillez contacter
            <a href="mailto:admin@colibriste.org">admin@colibriste.org</a>
          </p>

          <h2>Établissements de liens vers la plateforme Colibriste</h2>
          <p>
            Colibriste autorise la mise en place d'un lien hypertexte vers les services Colibriste pour tous les sites
            Internet, à l'exclusion de ceux diffusant du contenu pornographique, xénophobe ou pouvant, dans une plus
            large mesure porter atteinte à la sensibilité du plus grand nombre (racisme, sexisme, homophobie,
            transphobie, validisme, etc...).
          </p>
          <p>
            Les services Colibriste ne doivent en aucun cas être intégrées à l'intérieur des pages d'un autre site
            (iframe).
          </p>
          <p>
            Dans tous les cas d'espèce, Colibriste se réserve le droit de demander la suppression d'un lien si elle
            estime que le site cible ne respecte pas les règles ainsi définies ou s'il venait à mettre en péril la
            qualité du service par une augmentation trop forte de l'activité sur les services ne pouvant être absorbées
            par les serveurs.
          </p>

          <h2>Limitation de responsabilité</h2>
          <p>
            Ce site comporte des informations mises à disposition par des communautés ou sociétés externes ou des liens
            hypertextes vers d’autres sites qui n’ont pas été développés par le collectif Colibriste. Le contenu mis à
            disposition sur le site est fourni à titre informatif. L’existence d’un lien de ce site vers un autre site
            ne constitue pas une validation de ce site ou de son contenu. Il appartient à l’internaute d’utiliser ces
            informations avec discernement et esprit critique. La responsabilité du collectif Colibriste ne saurait être
            engagée du fait des informations, opinions et recommandations formulées par des tiers.
          </p>
          <p>
            Le collectif Colibriste ne pourra être tenu responsable des dommages directs et indirects causés au matériel
            de l’utilisateur, lors de l’accès au site, et résultant soit de l’utilisation d’un matériel ne répondant pas
            aux spécifications techniques requises, soit de l’apparition d’un bug ou d’une incompatibilité.
          </p>
          <p>
            Le collectif Colibriste ne pourra également être tenu responsable des dommages indirects (tels par exemple
            qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site.
          </p>
          <p>
            Des espaces interactifs (possibilité de poser des questions dans l’espace contact) sont à la disposition des
            utilisateurs sur le site du collectif Colibriste. Le collectif Colibriste se réserve le droit de supprimer,
            sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation
            applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant,
            Le collectif Colibriste se réserve également la possibilité de mettre en cause la responsabilité civile
            et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou
            pornographique, quel que soit le support utilisé (texte, photographie…).
          </p>
          <p>
            Le collectif Colibriste ne pourra être tenue responsable de dommages matériels liés à l’utilisation du site.
            De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas
            de virus et avec un navigateur de dernière génération mis-à-jour
          </p>

          <h2>Limitations contractuelles sur les données</h2>
          <p>
            Le collectif Colibriste décline toute responsabilité en cas de perte ou altération de vos données. La
            plateforme est gérée de façon bénévole et sur investissement personnel. Bien que des sauvegardes soient
            mises en place aucune garantie ne peut être faite sur la possibilité de restitutions de celles-ci. Nous vous
            incitons à faire des sauvegardes de vos données importantes de votre côté également.
          </p>
        </div>
      </div>
    `;
  }
}
