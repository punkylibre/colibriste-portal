module gitlab.com/punkylibre/colibriste-portal

go 1.20

require (
	github.com/etherlabsio/healthcheck/v2 v2.0.0
	github.com/jinzhu/gorm v1.9.16
	github.com/oschwald/maxminddb-golang v1.10.0
	golang.org/x/crypto v0.8.0
	golang.org/x/oauth2 v0.7.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
